
name := "akka-cloud-client"

organization := "com.solarsuit"

version := "1.0"

scalaVersion := "2.12.0"

enablePlugins(ScalaJSPlugin)

libraryDependencies ++= Seq(
  "org.scala-js" %%% "scalajs-dom" % "0.9.1",
  "com.lihaoyi" %%% "autowire" % "0.2.6",
  "io.suzaku" %%% "boopickle" % "1.2.6",
  "com.solarsuit" %%% "akka-cloud-common" % "1.0-SNAPSHOT"
)
        