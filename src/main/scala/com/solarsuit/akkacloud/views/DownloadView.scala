package com.solarsuit.akkacloud.views

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import com.solarsuit.akkacloud.views.manage.dialogs.Dialog
import org.scalajs.dom
import org.scalajs.dom.ext.Ajax

import scala.scalajs.js.JSApp

/**
  * Created by arsenii on 30.03.17.
  */
object DownloadView extends JSApp with ViewHelper {
  override def main(): Unit = {
    val container = dom.document.body add "div" attr "class" -> "container-fluid text-center"
    container.get.style.visibility = "hidden"

    val href = dom.window.location.href
    val id = href.split("/").last
    val link = s"direct/$id"
    val response = Ajax.get(s"type/$id").map(_.responseText)

    response.map(_.split(":")).foreach(meta => {
      container add "h4" text "File name:"
      container add "p" text meta.head
      container add "h4" text "File preview:"
      meta.tail.head match {
        case s if s.startsWith("video/") => container add "video" attr "controls"->"true" add "source" attr("src"->link,"type"->s)
        case s if s.startsWith("audio/") =>container add "audio" attr "controls"->"true" add "source" attr("src"->link,"type"->s)
        case s if s.startsWith("application/pdf") => container add "embed" attr("src"->link,"type"->"application/pdf")
        case s if s.startsWith("image/") =>container add "img" attr("src"->link,"class"->"img-responsive")
        case _ => container add "p" text  "preview is not available"
      }
      container add "div" add "a" attr ("class"->"btn btn-s btn-info","href"->link) text "download"
      removeSpinner()
      container.get.style.visibility = "visible"
    })
    response.failed.foreach(m => Dialog.showInfo(text=m.toString))

  }
}
