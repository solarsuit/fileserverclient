package com.solarsuit.akkacloud.views.helpers

import boopickle.Default._
import com.solarsuit.akkacloud.views.manage.dialogs.Dialog
import scalajs.concurrent.JSExecutionContext.Implicits.queue
import org.scalajs.dom
import org.scalajs.dom.{Attr, Event}
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.MouseEvent

import scala.concurrent.Future
/**
  * Created by arsenii on 17.04.17.
  */
trait ViewHelper {
  protected def doAfterOption[T](f: Future[Option[T]])(action:T=>Unit)={
    f.foreach{
      case Some(b) => action(b)
      case None => Dialog.showInfo(text="error during query execution")
    }
    f.failed.foreach{
      f =>  Dialog.showInfo(text=f.toString)
    }
  }

  protected def doAfter[T](f: Future[T])(action:T=>Unit)={
    f.foreach{b=>action(b)}
    f.failed.foreach{
      f =>  Dialog.showInfo(text=f.toString)
    }
  }

  protected def escapeHtml(in:String):String = {
    in.replaceAll("<|>|../","")
  }

  protected def removeSpinner():Unit = {
    val el = dom.document.getElementById("spinner")
    el.parentNode.removeChild(el)
  }

  implicit class NodeWrapper(node:Element){
    def get: Element = node

    def text(text:String): NodeWrapper = {
      node.textContent=text
      this
    }

    def onClick(event:MouseEvent=>Unit):NodeWrapper = {
      node.onclick = event
      this
    }

    def onChange(event:Event=>Unit):NodeWrapper = {
      node.onchange = event
      this
    }

    def tooltip(text:String): NodeWrapper ={
      this attr("data-toggle"->"tooltip",
      "data-placement"->"top",
      "title"->text)
    }

    def attr(attribute:(String,String)*):NodeWrapper ={
      attribute.foreach(attr=>node.setAttribute(attr._1,attr._2))
      this
    }
    def class_(attribute:String):NodeWrapper ={
      node.setAttribute("class",attribute)
      this
    }

    def add(element: Element):NodeWrapper={
      node.appendChild(element)
      element
    }

    def add(elemName:String) : NodeWrapper ={
      val newNode:Element = dom.document.createElement(elemName).asInstanceOf[Element]
      node.appendChild(newNode)
      newNode
    }
  }

}
