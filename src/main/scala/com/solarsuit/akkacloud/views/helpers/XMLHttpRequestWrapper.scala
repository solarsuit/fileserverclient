package com.solarsuit.akkacloud.views.helpers

import com.solarsuit.akkacloud.views.manage.dialogs.ProgressModal
import org.scalajs.dom
import org.scalajs.dom.ext.Ajax.InputData
import org.scalajs.dom.ext.{Ajax, AjaxException}
import org.scalajs.dom.raw.ProgressEvent

import scala.concurrent.{Future, Promise}

/**
  * Created by arsenii on 04.05.17.
  */
object XMLHttpRequestWrapper {


  def get(url: String, data: InputData = null, timeout: Int = 0,
          headers: Map[String, String] = Map.empty,
          withCredentials: Boolean = false, responseType: String = "",description:String="downloading file") = {
    apply("GET", url, data, timeout, headers, withCredentials, responseType,ProgressModal.addRequest(description))
  }

  def post(url: String, data: InputData = null, timeout: Int = 0,
           headers: Map[String, String] = Map.empty,
           withCredentials: Boolean = false, responseType: String = "",description:String="uploading file") = {
    apply("POST", url, data, timeout, headers, withCredentials, responseType,ProgressModal.addRequest(description))
  }

  def put(url: String, data: InputData = null, timeout: Int = 0,
          headers: Map[String, String] = Map.empty,
          withCredentials: Boolean = false, responseType: String = "",description:String="waiting for api responce") = {
    apply("PUT", url, data, timeout, headers, withCredentials, responseType,ProgressModal.addRequest(description))
  }

  def delete(url: String, data: InputData = null, timeout: Int = 0,
             headers: Map[String, String] = Map.empty,
             withCredentials: Boolean = false, responseType: String = "",description:String="deleting file") = {
    apply("DELETE", url, data, timeout, headers, withCredentials, responseType,ProgressModal.addRequest(description))
  }

  def apply(method: String, url: String, data: InputData, timeout: Int,
            headers: Map[String, String], withCredentials: Boolean,
            responseType: String,setProgress:ProgressEvent=>Unit): Future[dom.XMLHttpRequest] = {
    val req = new dom.XMLHttpRequest()
    val promise = Promise[dom.XMLHttpRequest]()
    req.onprogress =  setProgress
    req.onreadystatechange = { (e: dom.Event) =>
      if (req.readyState == 4) {
        if ((req.status >= 200 && req.status < 300) || req.status == 304)
          promise.success(req)
        else
          promise.failure(AjaxException(req))
      }
    }
    req.open(method, url)
    req.responseType = responseType
    req.timeout = timeout
    req.withCredentials = withCredentials
    headers.foreach(x => req.setRequestHeader(x._1, x._2))
    if (data == null)
      req.send()
    else
      req.send(data)
    promise.future
  }
}
