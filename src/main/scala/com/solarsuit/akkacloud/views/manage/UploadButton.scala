package com.solarsuit.akkacloud.views.manage

import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.{ViewHelper, XMLHttpRequestWrapper}
import com.solarsuit.akkacloud.views.manage.dialogs.{Dialog, ProgressModal}
import org.scalajs.dom
import org.scalajs.dom.FileList

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.html.{Element, Form, Input}
import org.scalajs.dom.raw.FormData

import scala.scalajs.js.Date

/**
  * Created by arsenii on 25.04.17.
  */
object UploadButton extends ViewHelper{

  def apply(fileRepresentation: FileRepresentation) = {

    //upload
    val form = dom.document.body add "form" attr(
      "action"->("api/"+fileRepresentation.meta.path),
      "method"->"post",
      "enctype"->"multipart/form-data",
      "target"->"_blank"
    )

    val in = form add "input" attr (
      "type"->"file",
      "required"->"true",
      "multiple"->"true",
      "name"->"fileToUpload",
      "id"->"fileToUpload"
    )
    in onChange (_ => {
      val input = in.get.asInstanceOf[Input]
      val result =XMLHttpRequestWrapper.post(
        url = "api/"+fileRepresentation.meta.path,
        data = Ajax.InputData.formdata2ajax(new FormData(form.get.asInstanceOf[Form])),
        description = "Uploading of "+input.value
      ).map(result=>result.responseText)
      doAfter(result)(result=>{
        val res = dom.document.createElement("div").asInstanceOf[Element]
        result.split("~!~").filter(!_.isEmpty).foreach(s=> res add "p" text s)
        Dialog.showInfoHtml("Upload summary:",res)
        if(fileRepresentation.opened) {
          val fileList = input.files
          if(fileList.length>0)//TODO:don't add files that failed loading
            walkFileList(fileList,fileList.length-1,fileRepresentation)
        }
      })
      form.get.asInstanceOf[Form].reset()
    })

    fileRepresentation.nameSection add "button" attr "class"->"btn btn-default btn-s" onClick
      (_=> in.get.asInstanceOf[Input].click()) add "span" attr "class"->"glyphicon glyphicon-upload"

  }


  def walkFileList(list:FileList,index:Int,fileRepresentation: FileRepresentation):Unit={
    val file =list.item(index)
    val path =fileRepresentation.meta.path+"/"+file.name
    val mimeType = file.`type`
    FileRepresentation(fileRepresentation.childsRootDiv,FileView(path,isDir = false,Date.now.toLong,None,mimeType))
    if(index>0)
      walkFileList(list,index-1,fileRepresentation)
  }

}
