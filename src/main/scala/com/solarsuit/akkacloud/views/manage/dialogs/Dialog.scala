package com.solarsuit.akkacloud.views.manage.dialogs

import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom
import org.scalajs.dom.html.Element

/**
  * Created by arsenii on 23.04.17.
  */
object Dialog extends ViewHelper{

  val id ="modalConformationDialog"

  protected def title: Element = dom.document.getElementById("conformationHeader").asInstanceOf[Element]
  protected def text: Element = dom.document.getElementById("conformationText").asInstanceOf[Element]
  protected def fire: Element = dom.document.getElementById("conformationAction").asInstanceOf[Element]

  private def show = scalajs.js.eval("$('#"+id+"').modal('show');")

  def showInfo(title:String="Error",text:String ="Something went wrong"):Unit={
      fire.style.visibility = "hidden"
      this.title text title
      this.text text text
      show
  }

  def showInfoHtml(title:String="Error",text:Element):Unit={
    fire.style.visibility = "hidden"
    this.text.textContent=""
    this.title text title
    this.text add text
    show
  }

  def showConformation(action:()=>Unit,title:String="confirm action",text:String ="Are you sure?",buttonText:String="Yes"):Unit =
  {
      fire.style.visibility = "visible"
      this.title text title
      this.text text text
      fire text buttonText onClick (_=>action())
      show
  }

  def showConformationHtml(action:()=>Unit,title:String="confirm action",text:Element,buttonText:String="Yes"):Unit = {
    fire.style.visibility = "visible"
    this.title text title
    this.text.textContent=""
    this.text add text
    fire text buttonText onClick (_=>action())
    show
  }
}
