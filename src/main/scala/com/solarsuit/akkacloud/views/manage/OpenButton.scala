package com.solarsuit.akkacloud.views.manage

import autowire._
import boopickle.Default._
import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.html.{Element, Form, Input}
import org.scalajs.dom.raw.{FormData, MouseEvent}
import scala.concurrent.Future

/**
  * Created by arsenii on 18.04.17.
  */
case class OpenButton(button:Element,glyph: Element,f:FileRepresentation) extends ViewHelper{

  button onClick onclick

  private def onclick =(x: MouseEvent) =>{
    if(!f.opened) {
      glyph attr "class"->"glyphicon glyphicon-folder-open"
      button class_ "btn btn-default btn-s disabled"
      f.createSubelements(()=>button class_ "btn btn-default btn-s")
    }else{
      glyph attr "class"->"glyphicon glyphicon-folder-close"
      f.removeSubelements()
    }
  }

}

object OpenButton extends ViewHelper{

  def apply(file:FileRepresentation):OpenButton ={
    val button = file.nameSection add "button" attr "class"->"btn btn-default btn-s"
    val glyphSpan = button add "span" attr "class"->"glyphicon glyphicon-folder-close"
    button add "span" text file.meta.name

    new OpenButton(button.get,glyphSpan.get,file)
  }
}