package com.solarsuit.akkacloud.views.manage

import autowire._
import boopickle.Default._

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.controllers.helpers.{Manage, WireClient}
import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import com.solarsuit.akkacloud.views.manage.dialogs.Dialog
import org.scalajs.dom
import org.scalajs.dom.html.{Element, Input}

import scala.scalajs.js.Date

/**
  * Created by arsenii on 26.04.17.
  */
object MkdirButton extends ViewHelper{

  def apply(fileRepresentation: FileRepresentation) ={

    val collapsable = dom.document.createElement("div").asInstanceOf[Element] attr "class"->"input-group"

    val inputPre = collapsable add "input" attr(
      "type"->"text",
      "class"->"form-control",
      "size"->"15"
    ) tooltip "enter the name of a folder to create"

    val input = inputPre.get.asInstanceOf[Input]

    val action = ()=> {
        val inputVal = input.value
        if(inputVal.isEmpty) {
          Dialog.showInfo(text="can not be empty")//this wouldn't work
        }else {
          doAfterOption(WireClient[Manage].mkdir(createPath(fileRepresentation.meta.path, inputVal)).call())(
            _ => {
              if(fileRepresentation.opened){
               FileRepresentation(
                 fileRepresentation.childsRootDiv,
                 FileView(fileRepresentation.meta.path+"/"+inputVal,
                   isDir = true,Date.now.toLong,
                   None,
                   "application/zip"
                 )
               )
              }
              input.value = ""
            }
          )
        }
      }

    val b = fileRepresentation.nameSection add "button" attr ("type"->"button","class"->"btn btn-secondary")
    b add "span" attr "class"->"glyphicon glyphicon-plus"
    b onClick(_=>Dialog.showConformationHtml(action,"create folder",collapsable.get,"Create"))
  }

  def createPath(base:String, end:String):String={
    if(base.isEmpty)
      end
    else
      base+"/"+end
  }
}
