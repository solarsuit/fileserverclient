package com.solarsuit.akkacloud.views.manage

import autowire._
import boopickle.Default._

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.controllers.helpers.{Manage, WireClient}
import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom.html.Element
/**
  * Created by arsenii on 18.04.17.
  */

case class FileRepresentation(parent:Element,f:FileView)extends ViewHelper{

  val root: Element = parent add "div" attr("class"->"row") get
  val nameSection: Element = root add "div" attr "class"->"col-xs-4" get
  val linkSection: Element = root add "div" attr "class"->"col-xs-4" get
  val buttonSection: Element = root add "div" attr "class"->"col-xs-4" get
  val childsRootDiv: Element = parent add "div" attr "class"->"col-xs-offset-1" get
  def meta: FileView = f

  if(f.isDir) {
    if(f.path!="") OpenButton(this)
    UploadButton(this)
    MkdirButton(this)
  }else
    nameSection text f.name

  if(f.path!="") {
    ShareButton(linkSection, buttonSection, f)
    DeleteButton(buttonSection, root, childsRootDiv, f.path)
    DownloadButton(buttonSection, f.path)
  }

  var opened = false

  def createSubelements(callback:()=>Unit = ()=>Unit):Unit = {
    doAfterOption(WireClient[Manage].getFileList(f.path).call()){b=>
      b.foreach(f => FileRepresentation(childsRootDiv,f))
      opened=true
      callback()
    }
  }

  def hasSubelements:Boolean = childsRootDiv.childElementCount>0

  def hideSubelements():Unit = {
    opened=false
    childsRootDiv.style.visibility="hidden"
  }

  def showSubelements():Unit = {
    opened=true
    childsRootDiv.style.visibility="visible"
  }

  def removeSubelements():Unit={
    childsRootDiv.textContent=""
    opened=false
  }

}
