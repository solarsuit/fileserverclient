package com.solarsuit.akkacloud.views.manage

import autowire._
import boopickle.Default._

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.controllers.helpers.{Manage, WireClient}
import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.MouseEvent
/**
  * Created by arsenii on 17.04.17.
  */
case class ShareButton(element:Element,link:Element,path:String) extends ViewHelper{

  private val glyph = element add "span" attr "class"->"glyphicon glyphicon-share"
  private val text = element add "span" text "share"

  def setToShare():Unit = {
    element attr "class"-> "btn btn-info btn-s"
    element onClick ((x:MouseEvent) =>share())
  }

  def setToUnshare():Unit = {
    element attr "class"-> "btn btn-info btn-s active"
    element onClick ((x:MouseEvent) => unshare())
  }

  private def share():Unit = {
    doAfterOption(WireClient[Manage].share(path).call()) {
      str => {
        link text ShareButton.sharedText
        setToUnshare()
        link attr "href"->("/download/" + str) //TODO:give direct link if preview is not available?
      }
    }
  }

  private def unshare():Unit={
    doAfterOption(WireClient[Manage].unShare(path).call()){
      _ => {
        link text ShareButton.emptyText
        setToShare()
        link.get.removeAttribute("href")
      }
    }
  }
}

object ShareButton extends ViewHelper{

  protected val sharedText = "public download link"
  protected val emptyText = "not shared"

  def apply(linkSection:Element,buttonSection:Element, f:FileView): ShareButton ={
    val link = linkSection add "a" attr "target"->"_blank"
    if(f.link.isDefined)
      link attr "href"->f.link.get text sharedText
    else
      link text emptyText
    val out = new ShareButton(buttonSection add "button" get, link get, f.path)
    if(f.link.isDefined) out.setToUnshare() else out.setToShare()
    out
  }
}