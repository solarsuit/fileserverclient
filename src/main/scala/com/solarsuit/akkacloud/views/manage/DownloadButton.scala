package com.solarsuit.akkacloud.views.manage

import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom
import org.scalajs.dom.html.{Element, Input}

/**
  * Created by arsenii on 23.04.17.
  */
object DownloadButton extends ViewHelper{

  def apply(root:Element,path:String) ={
    val submit = dom.document.body add "form" attr(
      "action"->("api/"+path),
      "method"->"get"
    ) add "input" attr "type"->"submit"

    val button =  root add "button" attr "class"->"btn btn-default btn-s" onClick
      (_=> submit.get.asInstanceOf[Input].click())

    button add "span" attr "class"->"glyphicon glyphicon-download"
    button add "span" text "download"
  }
}
