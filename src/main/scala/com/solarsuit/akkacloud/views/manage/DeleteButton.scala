package com.solarsuit.akkacloud.views.manage

import autowire._
import boopickle.Default._

import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.controllers.helpers.{Manage, WireClient}
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import com.solarsuit.akkacloud.views.manage.dialogs.Dialog
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.MouseEvent

/**
  * Created by arsenii on 23.04.17.
  */

object DeleteButton extends ViewHelper{

  def text(path:String) =s"You sure you want to delete file at $path? This action cannot be undone!"

  def apply(buttonSection:Element, root:Element,childsRoot:Element, path:String)= {
    val button = buttonSection add "button" attr "class" -> "btn btn-warning btn-s"
    button add "span" attr "class"-> "glyphicon glyphicon-remove-circle"
    button add "span" text "delete"
    val action = () => doAfterOption(WireClient[Manage].delete(path).call())(_=>{childsRoot.parentNode.removeChild(childsRoot);root.parentNode.removeChild(root)})
    button onClick (_=> Dialog.showConformation(action,"are you sure?",text(path)))
  }
}
