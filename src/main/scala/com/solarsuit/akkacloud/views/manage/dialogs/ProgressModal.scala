package com.solarsuit.akkacloud.views.manage.dialogs

import com.solarsuit.akkacloud.views.helpers.ViewHelper
import org.scalajs.dom
import org.scalajs.dom.html.Element
import org.scalajs.dom.raw.ProgressEvent

/**
  * Created by arsenii on 04.05.17.
  */
object ProgressModal extends ViewHelper{
  val id = "progressModal"
  val textId="progressModalText"

  def open = scalajs.js.eval("$('#"+id+"').modal({backdrop: false});")
  def close = scalajs.js.eval("$('#"+id+"').modal('hide')")
  def addRequest(description:String):ProgressEvent=>Unit ={
    open
    val part = dom.document.getElementById(textId).asInstanceOf[Element] add "div"
    part add "p" text description
    val progressbar = part add "div" class_ "progress" add "div" class_ "progress-bar"
    (e:ProgressEvent)=>{
      progressbar attr ("aria-valuemax"->e.total.toString,"aria-valuemin"->0.toString,"aria-valuenow"->e.loaded.toString)
      if(e.total==e.loaded){
        //clear when finished
        part.get.parentNode.removeChild(part.get)
      }
      if(dom.document.getElementById(textId).asInstanceOf[Element].childElementCount==0){
        //close when no progress bars left
        close
      }
    }
  }



}
