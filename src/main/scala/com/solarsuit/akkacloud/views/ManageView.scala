package com.solarsuit.akkacloud.views

import autowire._
import boopickle.Default._
import scalajs.concurrent.JSExecutionContext.Implicits.queue
import com.solarsuit.akkacloud.controllers.helpers.Manage
import com.solarsuit.akkacloud.models.orm.FileView
import com.solarsuit.akkacloud.views.helpers.ViewHelper
import com.solarsuit.akkacloud.views.manage.{FileRepresentation, MkdirButton, UploadButton}
import scala.scalajs.js.JSApp
import org.scalajs.dom
import org.scalajs.dom.html.Element
/**Element
  * Created by arsenii on 27.03.17.
  */

object ManageView extends JSApp with ViewHelper {

  override def main(): Unit = {
    val container = dom.document.body add "div" attr "class"->"container-fluid"
    container.get.style.visibility="hidden"
    val rootFile = FileView("",isDir = true,0,None,"application/zip")
    FileRepresentation(container.get,rootFile).createSubelements(
      ()=>{removeSpinner();container.get.style.visibility="visible"}
    )
  }

}

