package com.solarsuit.akkacloud.controllers.helpers

import java.nio.ByteBuffer

import autowire._
import boopickle.Default._
import org.scalajs.dom

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.typedarray.{ArrayBuffer, Int8Array}
/**
  * Created by arsenii on 31.03.17.
  */
object WireClient extends WireAbsractClient{

  override def doCall(req: WireClient.Request): Future[ByteBuffer] = {
    dom.ext.Ajax.put(
      url = "/api/" + req.path.mkString("/"),
      data = Pickle.intoBytes(req.args),
      responseType = "arraybuffer"
    ).map(result=>{
      val res = result.response.asInstanceOf[ArrayBuffer]
      ByteBuffer.wrap(new Int8Array(res).toArray)
    })
  }

}
